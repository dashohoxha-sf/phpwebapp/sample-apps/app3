<?php
/* This file is part of phpWebApp. */

/**
 * This file is included the first time that the application
 * is opened (when the session starts).
 */

WebApp::message("Wellcome to Sample Application 3");

if (COUNT_CLICKS)
{
  WebApp::addSVar("click_nr", "0"); 
}
?>
