<?php
/* This file is part of phpWebApp. */

/**
 * This file is included every time. It contains functions,
 * constants and variables that are used throughout the
 * whole application.
 */

if (COUNT_CLICKS)
{
  $click_nr = WebApp::getSVar("click_nr");
  $click_nr++;
  WebApp::message("This is click number $click_nr in this session.");
  WebApp::setSVar("click_nr", $click_nr);
}
?>