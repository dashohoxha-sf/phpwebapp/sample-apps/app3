//-*- mode: C; -*-//tells emacs to use mode C for this file
/* This file is part of phpWebApp. */

/**
 * This file contains the JavaScript code
 * of the WebBox 'menu'.
 */
/** */

function goto_page1()
{
  GoTo("page1.html");
}

function goto_page2()
{
  GoTo("page2.html");
}

function goto_page3()
{
  GoTo("page3.html");
}

function go_back()
{
  history.back();
}
