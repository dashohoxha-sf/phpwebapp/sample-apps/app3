<?php
/* This file is part of phpWebApp. */

class menu extends WebObject
{
  function onRender()
    {
      $date = date("M d, Y");
      WebApp::addVar("date", $date);
    }
}
?>