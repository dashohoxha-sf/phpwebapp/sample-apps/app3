<?php
/* This file is part of phpWebApp. */

include_once TIP_PATH."fun.get_random_tip.php";

class tip3 extends WebObject
{
  function onRender()
    {
      //get a random tip
      $tip = get_random_tip();
      $r = rand(1, 5);  //random value from 1 to 5

      $td2 = "<td class='tip_style_$r'>$tip</td>\n";
      WebApp::addVar("td2", $td2);

      /*
       * There is still some HTML code constructed here.
       * This is not a good practice, because one of the aims of the
       * framework is to separate the PHP and HTML codes, and it
       * makes it possible to separate them almost 100%.
       */
    }
}
?>