<?php
/* This file is part of phpWebApp. */

include_once TIP_PATH."fun.get_random_tip.php";

class tip2 extends WebObject
{
  function onRender()
    {
      //get a random tip
      $tip = get_random_tip();
      $r = rand(1, 5);  //random value from 1 to 5

      $html =  "
        <table class='tip'>
          <tr>
            <td class='tip_name' valign='top' width='30'>Tip2:</td>
            <td class='tip_style_$r'>$tip</td>
          </tr>
        </table>
        ";
      WebApp::addVar("tip_html_content", $html);

      /*
       * This webbox constructs all the HTML code of the webbox here
       * and then outputs all of it in a single {{variable}}.
       * This is not a good practice, because one of the aims of the
       * framework is to separate the PHP and HTML codes, and it
       * makes it possible to separate them almost 100%.
       */
    }
}
?>