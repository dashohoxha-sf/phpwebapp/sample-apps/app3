<?php
/* This file is part of phpWebApp. */

include_once TIP_PATH."fun.get_random_tip.php";

class tip1 extends WebObject
{
  function onRender()
    {
      //get a random tip
      $tip = get_random_tip();
      $r = rand(1, 5);  //random value from 1 to 5

      //print the HTML code of the tip
      print "
        <table class='tip'>
          <tr>
            <td class='tip_name' valign='top' width='30'>Tip1:</td>
            <td class='tip_style_$r'>$tip</td>
          </tr>
        </table>
        ";

      /*
       * Basically you can print anything from the function onRender()
       * and it will be outputed before the HTML code of the WebBox.
       *
       * However:
       *
       * 1 - This is not necessary, except in very rare cases, because
       *     there are other means to construct the HTML code.
       *
       * 2 - It is not a good practice, because one of the aims of the
       *     framework is to separate the PHP and HTML codes, and it
       *     makes it possible to separate them almost 100%.
       *
       * 3 - Sometimes it may not produce the desired result. By default
       *     the framework sends immediately to the browser each line 
       *     of HTML code that it constructs. However, it can be instructed
       *     to accumulate all the generated HTML page, and then send it.
       *     The output of the 'print' function is sent directly to the
       *     browser and the framework has no way to control it. In this
       *     case, the output would not be what was expected.
       */
    }
}
?>