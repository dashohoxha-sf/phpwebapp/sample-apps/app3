<?php
/* This file is part of phpWebApp. */

/**
 * Returns a random tip form the tips in 'tips.php'.
 * 'tips.php' contains the array $arr_tips with tips.
 */
function get_random_tip()
{
  //include the file 'tips.php' which contains the array $arr_tips
  include TIP_PATH."tips.php";

  //get a random value from this array
  $max_idx = sizeof($arr_tips) - 1;
  $idx = rand(0, $max_idx);
  $tip = $arr_tips[$idx];

  return $tip;
}
?>