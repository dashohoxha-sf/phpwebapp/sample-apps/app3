<?php
/* This file is part of phpWebApp. */

/**
 * The array $arr_tips contains a list of tips, from which
 * one is chosen randomly.
 */
$arr_tips = array 
(
 "Never print anything from the PHP code, except maybe for debug!",

 "Never construct HTML code in the PHP, "
 . "put all the HTML in the template instead!",

 "Assign to template variables only a value that is really a variable,"
 . "what is not a variable should be placed in the template.",

 "It is recomended to place all the files related to a WebBox in a separate "
 . "folder, for better readability and structure of the application.",

 "It is recomended to place each WebBox in a separate template, "
 . "without other HTML code that does not belong to it.",

 "It is recomended to name the template of a WebBox with the same name "
 . "as the ID of the WebBox (and the extenssion 'html').",

 "All the files of a WebBox (.js, .css, .db, etc.) must have the "
 . "same name as the ID of the WebBox, otherwise the framework will not "
 . "reckognize them as belonging to this WebBox.",

 "There must not be two WebBox-es in the same web application with "
 . "the same ID!"
 );
?>