<?php
/* This file is part of phpWebApp. */

include_once TIP_PATH."fun.get_random_tip.php";

class tip extends WebObject
{
  function onRender()
    {
      //get a random tip and assign it to the template variable
      $tip = get_random_tip();
      WebApp::addVar("tip_text", $tip);
      
      //there are 5 styles, choose one of them randomly
      WebApp::addVar("rand", rand(1, 5) );
    }
}
?>