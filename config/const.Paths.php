<?php
/* This file is part of phpWebApp. */

//constants of the paths in the application
define("WEBAPP_PATH", UP_PATH."web_app/");
define("TPL",         APP_PATH."templates/");
define("TPL_URL",     APP_URL."templates/");
define("GRAPHICS",    APP_URL."graphics/");

define("TIP_PATH",    TPL."tip/");
?>
